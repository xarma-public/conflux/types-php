<?php

namespace Xarma\Conflux\Type;

class Contact extends \Xarma\Conflux\Source\Source\SourceObject
{
    public const TYPE = 'contact';

    public const CONTACT_TYPE_ORGANIZATION = 'organization';
    public const CONTACT_TYPE_PERSON = 'person';

    public const ALLOWED_CONTACT_TYPES = [self::CONTACT_TYPE_ORGANIZATION, self::CONTACT_TYPE_PERSON];

    private ?string $id = null;

    protected ?string $salutation = null;

    protected ?string $title = null;

    protected ?string $firstNames = null;

    protected ?string $middleNames = null;

    protected ?string $lastNames = null;

    protected ?string $preferredName = null;

    protected ?string $nickname = null;

    protected ?string $emailAddress = null;

    protected ?bool $optedIn = false;

    private string $contactType;

    public function __construct(string $contactType, private string $source, private ?\JsonSerializable $original)
    {
        $this->setContactType($contactType);
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getContent(): object
    {
        return (object)[
            'type' => $this->contactType,
            'title' => $this->title,
            'salutation' => $this->salutation,
            'first_names' => $this->firstNames,
            'middle_names' => $this->middleNames,
            'last_names' => $this->lastNames,
            'preferred_name' => $this->preferredName,
            'nickname' => $this->nickname,
            'email' => $this->emailAddress,
            'original' => $this->original,
            'opted_in' => $this->optedIn
        ];
    }

    protected function getMap(): array
    {
        return [
            'title' => 'title',
            'salutation' => 'salutation',
            'first_names' => 'firstNames',
            'middle_names' => 'middleNames',
            'last_names' => 'lastNames',
            'preferred_name' => 'preferredName',
            'nickname' => 'nickname',
            'email' => 'emailAddress',
            'opted_in' => 'optedIn'
        ];
    }

    public function setContactType(string $type)
    {
        $type = strtolower($type);
        if (!in_array($type, self::ALLOWED_CONTACT_TYPES)) {
            throw new \InvalidArgumentException("Unknown contact type {$type}");
        }

        $this->contactType = $type;
    }
}